<?php

/**
 * Fungsi untuk mengecek apakah user sudah login
 * atau belum lewat session
 */
function isLogin(){
	$CI = &get_instance();
	if(!$CI->session->userdata('is_logged_in')){
		redirect("login");
		die();
	}
}

function isSuperAdmin(){
	isLogin();
	$CI = &get_instance();
	if($CI->session->userdata('user_role') != "superadmin"){
		redirect("errors/forbidden");
		die();
	}
}
function isAdmin(){
	isLogin();
	$CI = &get_instance();
	if($CI->session->userdata('user_role') != "admin"){
		redirect("errors/forbidden");
		die();
	}
}

function isSuperAdminOrAdmin(){
	isLogin();
	$CI = &get_instance();
	if($CI->session->userdata('user_role') != "admin" ||
		$CI->session->userdata('user_role') != "superadmin"){

		redirect("errors/forbidden");
		die();
	}
}

function isUser(){
	isLogin();
	$CI = &get_instance();
	if($CI->session->userdata('user_role') != "user"){
		redirect("errors/forbidden");
		die();
	}
}
