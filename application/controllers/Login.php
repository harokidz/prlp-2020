<?php


class Login extends CI_Controller {

	/**
	 * Login constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model("UsersModel");
	}

	public function index() {
		$this->load->view("login");
	}

	public function proses_login() {
		$email = $this->input->post('email');
		$password = $this->input->post("password");
		$user = $this->UsersModel->getByEmailAndPassword($email, $password);
		if ($user == null) {
			$this->error_login("Email atau password tidak di temukan");
		} else {
			//user ada
			if ($user->is_active == "0") {
				$this->error_login("Silahkan aktivasi email anda");
			} else {
				//start session
				$dataSession = array(
					"user_id" => $user->id,
					"user_nama" => $user->nama,
					"user_role" => $user->role,
					"is_logged_in" => true
				);
				$this->session->set_userdata($dataSession);
				redirect("dashboard");
			}
		}
	}

	public function error_login($pesan) {
		echo $pesan;
	}
}
