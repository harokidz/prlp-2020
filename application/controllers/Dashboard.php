<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();
		isLogin();
	}

	public function index() {
		echo "Ini adalah dashboard";
	}

	public function coba() {
		isUser();
		echo "Anda seharusnya superadmin";
	}

	public function testing() {
		isSuperAdmin();
	}
}
