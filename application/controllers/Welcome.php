<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("UsersModel");
		isLogin();
	}

	public function index() {
		$customer = array(
			"name" => "Bowo",
			"email" => "pieter.haro@ukrim.ac.id",
			"kode_otp" => "562447"
		);
//		$this->sendEmail($customer);
		$this->load->view('welcome_message');
	}

	public function register() {
		$this->load->view("register");
	}

	public function prosesRegister() {
		$nama = $this->input->post("nama", true);
		$email = $this->input->post("email", true);
		$password = $this->input->post("password", true);
		$user = array(
			"nama" => $nama,
			"email" => $email,
			"password" => md5($password),
			"token" => md5($email)
		);
		$this->UsersModel->insert($user);
		$this->sendEmail($user);
	}

	public function aktivasi($token) {
		$user  = $this->UsersModel->getByToken($token);
		$data = array("is_active" => 1);
		$this->UsersModel->update($user->id,$data);
		redirect("login");
	}

	private function sendEmail($user) {
		$config = array(
			'useragent' => 'CodeIgniter',
			'protocol' => 'smtp',
			'mailpath' => '/usr/sbin/sendmail',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_user' => 'project.rpl333@gmail.com',
			'smtp_pass' => "asdfJKL123",
			'smtp_port' => 465,
			'smtp_keepalive' => TRUE,
			'smtp_crypto' => 'ssl',
			'wordwrap' => TRUE,
			'wrapchars' => 76,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'validate' => TRUE,
			'crlf' => "\r\n",
			'newline' => "\r\n",
		);

		$dataSend = array(
			"user" => $user
		);
		$body = $this->load->view('mail/mail_body', $dataSend, TRUE);
		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->from('project.rpl333@gmail.com', 'Pieter Haro');
		$this->email->to($user["email"]);
		$this->email->subject("Tugas Project RPL");
		$this->email->message($body);
		if ($this->email->send()) {
			return true;
		} else {
			echo $this->email->print_debugger();
			return false;
		}
//    }
	}


}
