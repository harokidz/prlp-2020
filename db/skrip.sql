create database prpl_register;
use prpl_register;

create table users(
  id int not null primary key auto_increment,
  email varchar(40) not null,
  nama varchar(40),
  password varchar(200),
  role enum("superadmin","admin","user") default "user",
  is_active tinyint default 0
);

alter table users add token varchar(200) after role;
